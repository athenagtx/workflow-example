<!-- markdownlint-disable MD036 MD041 -->

![Athena GTX](/img/athena_logo.png)

# Athena SW Dev Workflow

*for External Projects*

## Workflow

Workflow is split up into 2 proccesses

1. Implementing changes
2. Releasing a new version

### Implementing Changes

1. Document change being implemented in issue tracker
2. Create new develop/* branch
3. Implement change
4. Ensure all automated tests pass
5. Review. Will changes be released in the next update
6. If yes, start an ECR and mark issue as "Staged"
7. If no, mark issue as "Done"

```mermaid
graph TD;
idA>Change needs to be made]
idB(Document change in Issue Tracker)
idC(Create develop/* branch)
idD(Implement change on created branch)
idE(ENsure all automated tests pass)
idF{Will changes be released}
idG(Mark issues as Done)
idH(Create ECR)
idI(Mark issue as Staged)

idA --> idB
idB --> idC
idC --> idD
idD --> idE
idE --> idF
idF --no--> idG
idF --yes--> idH
idH --> idI
```

### Releasing a New Version

1. Create an RC/* branch from the develop/* branch
2. If there is more than one branch to included in the release, merge the RC/* branches into a new RC/* branch
3. Ensure **all** tests, automated and manual, pass
4. Tag version on RC/* branch
5. Once ECN is signed off, merge RC into Master
6. Remove rc tag from version number
7. Tag version number on Master branch
8. Prune develop/* and RC/* branches
9. Mark tasks as "Merged" in issue tracker

```mermaid
graph TD;
idA>Ready to Release Changes]
idB(Create RC/* branch from develop/* branch)
idC{Are there more develop branches to include}
idD(Merge develop branches into new RC branch)
idE{Are there more RC branches to include}
idF(Merge RC branches into new RC brnach)
idG(Ensure All tests pass)
idH(Tag version on RC branch)
idI(ECR/ECN Complete)
idJ(ECR Signed)
idK(Merge RC branch into Master)
idL(Renive RC letter from version number)
idM(TAG version on Master Branch)
idN(Prune RC and develop branches)
idO(Mark tasks as Released in issue tracker)

idA --> idB
idB --> idC
idC --yes--> idD
idC --no-->idE
idD --> idE
idE --yes-->idF
idE --no--> idG
idF --> idG
idG --> idH
idH --> idI
idI --> idJ
idJ --> idK
idK --> idL
idL --> idM
idM --> idN
idN --> idO
```

## Branch Names

* master -- functions as main release branch. It is not touched until we are releasing a new version to customers. **Only** merged with RC branch. No one works off the Master directly.
* releaseCandidate -- stable dev branch. Tracks closely with master branch. No one works off of this branch directly. Branch off of RC and merge back in when complete. Used for manual testing (SVRL). Can be used for external beta testing.
* develop/feature_descriptor -- adding a new feature or fixing a bug
* develop/fix_descriptor -- adding a new feature or fixing a bug
* demo/demoName_YEAR-- Demo branch for features wanted for show/demo. Acts as a release branch in conventional gitflow

### Branch Rules

* Only RC branches may merge into Master
* In the case that multiple RC branches are to be merged into Master, merge all staged RC branches into new RC branch.
* RC branches are to be pruned after being merged into Master
* Develop branches should be pruned after the resulting RC branch is merged into Master

## Merging

* Ensure all automated tests pass before and after merging back into ANY branch
* Ensure all automated and all manual tests (SVRL ect) pass before merging into master
* Ensure ECN is complete and signed off before merging into master

## Tags

* VERSION_TAGS live in Master and Develop branch only --> ex. v1.1.1
* DEMO_TAGS live in specific demo/or feature/dev branch --> ex. MHSRS_2017
* TESTING_TAG can live in feature or develop branches --> BETA_X_X

## Versioning

* Use Semantic Versioning
* MAJOR.MINOR.PATCH.optionalRCLetter

### Resources

* [gitflow](https://github.com/nvie/gitflow)
* [git branching](http://nvie.com/posts/a-successful-git-branching-model/)